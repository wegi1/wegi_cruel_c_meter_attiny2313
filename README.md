![Cruel_C_meter.jpg](https://bitbucket.org/repo/j88j59/images/3030445140-Cruel_C_meter.jpg)


**Capacitance meter with a current source to measure C from 1 pF to 400+ mF range**



a little movies

https://www.youtube.com/watch?v=98XM_6EsB6E


At the beginning of a huge thank to you for the Dillon from EasyEDA Team for helping to make this documentation. Without him, this description could not have been made.

Thank you very much Dillon. I'm owe You.


Scheme:

![C-meter_scheme.png](https://bitbucket.org/repo/j88j59/images/4150529975-C-meter_scheme.png)


**Features:**



This scheme is the result of many of my thoughts. For me, its beauty lies in its simplicity and effectiveness.


	The range of measured capacity from 1pF to over than 400 mF - However, be careful - such a measure as 400 mF would last over 500 seconds.


	Power supply AC or DC in the range of 9-12V - Yes. I specially added a diode bridge, not mistaken poles. Power supply is possible through the DC jack and installed a second connector for direct connection cables (just in case)


	Automatic switching range.

	
	Presentation of the measurement on the LCD display.


	Can be programmed in the circuit ISP. Pins are led out to the connector 

LCD display and also reset by the 100 ohm resistor.



**How is it working (electrical)**



I looked through such projects and noticed that often cooperates with 
the microcontroller 555. Here it functions as a whole taking a microcontroller.



**Power block** is simple. Used the bridge of diodes and stabilizer 7805. 
It is strongly recommended the installation of the heatsink for 7805. 
Filtering the supply voltage VCC is standard (C3, C4,C5,C6 - 100 nF and uF in pairs). 
Also standard processor has connected 100 nF capacitor on VCC (C7).

Also as in catalog is connected to the oscillator circuit. No any comment we need here. 
Reset is led to power by the 10K resistor. In addition, it is outputted to 
the microswitch by 1k resistor, as well it is led to pin header 
by the 100 ohm resistor for ISP possiblity.



**The brain** all of them is microcontroller ATtiny2313.



**interface functions**


PR5, PR6 - to determine the contrast of the display.

LCD connection:
PORTB.4,5,6,7 as data (btw. here is MOSI, MISO and SCK for ISP)
RS = PORTB.2
EN = PORTB.3



**a measure block**


PR1 - multi-turn potentiometer 1k to determine Vref at 4/5 Vcc. Brought to the end microcontroller PB1 (AIN1) analog comparator.

PR2,3,4 They are intended to calibrate each individual measuring channel. They are also multi-turn.

Transistors Q1 and Q2 operate in a current mirror. They are responsible for charging the tested capacitor constant current. To improve their thermal stability, joined them by heat-shrink.

Q3 transistor discharges capacitor, which was measured. It is controlled from the output of the processor PD4.


If you wonder why in the series to GND does not have with him 100 ohm resistor, note that the current base controls the output PD4 through resistor 3K that appropriate limits the current flowing through Q3. And that's why the resistor has not been chosen to use the next element in the circuit.


Transistors Q4, Q5, Q6 merely act as switches to select the measurement channel.
They are controlled by PORTD 5,3,2 of the processor. These ports are connected to the base through a resistor 1k. This time these transistors are connected in series with a resistor fixed and variable to make calibration channel. Then the selected one of these three channels charges the capacitor.
A charging time of the capacitor from 0V to Vref is the basis for the calculation of its capacity.
Thus, this simple circuit allows the measurement of even very small capacity.



**////////////////////////**
**Start-up and calibration**
**////////////////////////**


	You need to program ATtiny2313 file attached to the firmware Cmeter.hex

	After soldering, use the PR1 Vref set at 4/5 VCC.

	Use the PR5, PR6 to regulate LCD contrast.

	Then for each channel to be carried out to measure capacitors.

	For this you can use a capacitor model or a known (measured) capacity.

	For the first channel capacitor within 1000pF - 2200pF. Use the PR2.

	Channel within a second capacitor 1000nF - 2200 nF. Use the PR3.

	For the channel of the third capacitor within 220uF - 1000uF should be enough. Use the PR4.



**Now You can use it.**
**Enjoy**




**A bit deeper look...**



If you are not bored and you are interested in a deeper look at this, then let me explain what is this and how it work.

This is a capacity meter which works on the ATtiny2313 (probably is possible do it on the lot of AVR's). Range from 1pF to over 400 000 uF.



Firstly You must remeberanced this ver simply equation from school:


Uc = I*t/C


where
	Uc - voltage on the cappacitor [V]
	I - charging current [A]
	t - charging current [s]
	C - cappacity [F]

From this equation we can do ordinary transform for moving out of "t" first dividing both sides by "t" Uc/t = I/C.

OK now let's divide both sides by Uc so we gotta 1/t = I/C*Uc from this following that my friend:



**t = CUc/I**



Now you may wonder, what purpose I subscribe to here the transformation equations.

Had he wanted to boast that I know math? The reason is the opposite. I want to bypass the complicated mathematics in this project. And in a moment I'll prove it.

Okay now we must understand what exactly it is apparent from this equation.

This results in four things:

	1. It is a linear relationship. IF YOU CHARGE THE CAPACITOR WITH A CONSTANT CURRENT, THE INCREASE IN CHARGE IN THE CAPACITOR IS PROPORTIONAL TO THE CHARGING TIME.

	2. As a result, I could understand that the capacitor 100nF / 100V has ten times more cargo than the capacitor 100 nF / 10 V at the time when they are charged to their limits tensions.

	3. And this also shows that raising the charge voltage proportionally increases the loading time. This is very important !!!


And... So... finally...  **the our KEY arising from this:**


	If you know the charging voltage (from 0V to our known value Vref)
	If you know the charging current (MUST BE A CONSTANT !!! )
	If you know the charging time (from 0V to Vref)


Then you can determine what capacity is tested capacitor.


One condition is only here:

Charging MUST BE A CONSTANT CURRENT.

NOT CONSTANT VOLTAGE !!!



Well. Now let's get back to this simple equation and let me prove to you how I here miss complicated mathematics.


Our simple equation sounds exactly:
t = C*Uc/I

and that's all my mate.


Here we sea a oscilloscope diagram from this

![330nF IIIch.jpg](https://bitbucket.org/repo/j88j59/images/4046890275-330nF%20IIIch.jpg)



Typically, the capacitor is charged with voltage. So take a look at how mathematically described the process of this load.

Vc = Vcc*(1-exp^(-t/RC))

UPS... and don't even I'll try to describe this equation... This equation itself looks daunting enough.



Bellow a diagram from this (stolen from internet)

![charging by voltage.png](https://bitbucket.org/repo/j88j59/images/4091532588-charging%20by%20voltage.png)





And **here is my prove for You** why I exhibit off before - I can do translate a simple equation.





From the above considerations, although it does not result directly - it shows how you can measure the capacitance of the capacitor and how can we do it.

In other words - we can measure time capacitor charging from constant current source.

Can we ?
**Yes, we can !**


For this purpose we need:

	- analog comparator - is contained in the microcontroller ATtiny2313 (and in many other AVR)
	- a referential voltage for comparator - Vref - PR1 providing this
	- the current source - for this task are the transistors Q1 and Q2 that operate in current mirror
	- measuring time of load - Yeah - we gotta quartz and TIMERS !



...Okay what we must do in the microcontroller program tasks - what HE must do?

	- he must be able discharge measure capacitor
	- charge capacitor
	- counting time of load capacitor
	- serviced LCD 16*2 display
	- some times divide and multiply 32bit unsigned value by 8 bit unsigned value
	- convert the result into ASCII fixed point value
	- present on LCD our result
	- automatically change range of measure for bigger/smaller capacitors

That's why I do this in assembler, because this is a lot of tasks and translating this to C would be impossible to do in 2048 bytes flash ram only. The program at this moment have bellowed 1100 bytes from available 2048 bytes.



Okay - now I'll tell You how does it work from electrical side and from a program side.

We need once again a transformation:

t = C*Uc/I => t/C =Uc/I => C/t = I/Uc =>

=> **C = It/Uc**


Here we gain some benefits from properties in our circuits, because we have two times a constant values. We know before "I" and "Uc" and this dependence is important for the ease of calculations in the future.


The reference voltage was set at 4V. In the case of the first range, this gives 4 of clock ticks per one period (for 8MHZ quartz). Thus, the measurement takes place in this case with a resolution of 0.25 pF



If the reference voltage was 1V, then it would be one of clock ticks per period (for 8MHZ quartz). Here, the measurement would be made with a resolution of 1 pF.


Now do you get it ?
Now do you know why I set UP Vref till 4/5 Vcc ?


What does the program do with this hardware



**After reset**




The meter starts pulls the output to ground for stability, the test is set to enter the waiting and unloading, and then begins the calibration measurement sequentially for all three channels to compensate for own capacitance and delays the switching transistors.

If the calibration was successful, offsets are saved for each channel and displayed once on the display, as triplet (24bit), although they did so 3-8-bit values. If the switched capacitor was connected to greater than about 30 pF, then the offset values are stored data determined empirically.

Each time the measurement offset is checked and adjusted if necessary, if the result is negative - that automatically compensates for differences due to temperature changes on the transistors.

Making the measurement takes place when inserted into the terminals of the capacitor legs. Needless to say, that you should put a capacitor previously discharged, because you can end up badly for the microcontroller. :-)

During measuring it is checked whether the size is not too small and that the measure does not last too long, for too long the measurement (about 0.5 sec), if this is not the highest range, the measurement is automatically restarted on another channel.

After the measurement is checked if the value is too low for a given range, and if it is too low, the measuring range is adjusted.

If the value is not considered too low, it is transformed number of "clicks" the clock on the result comprehensible to us and is displayed on the LCD in two versions - as measured established capacity for the channel (and are picofarads for the first channel and appropriately nano farads and microfarads) and the second score line is converted over three decades, which may be helpful for large values. With a picofarads displayed below are nanofarads etc.

After the presentation of the result range is switched to the next measuring cycle, after 3 seconds it starts again. 3 seconds is long enough to read the results of our perception, further discharges at this time the capacitor and stabilizes the measuring system.

The range of measured capacity can be from 1 pF to 4294967295 "ticks" clock (32bit unsigned value), which after transformation gives 452,101 uF (such measurement would take 536 seconds). Of course, I do not have such large capacitors (nay - the measure did not have time to unload it!).



**The epilog**


I think it is worth mentioning that is notable in the procedure for obtaining the (conversion) number as a sequence of ASCII from 32bit unsigned value. It is a small, clever algorithm to divide by 10, and received the rest of the presentation. Is short, it is important. Procedures multiplication and division are done via software, because not everyone AVR (including ATtiny2313) has built-in commands divide and multiply. In this way it will work on any AVR.



If you want to know more about the operation of the system, the rest of the search for the source ASM file. In the comments and how I named labels.




**Thank you for your attention dear reader.**
**Best regards - wegi**




//----
//todo
// Function for large capacitors, which is taken into account their discharge time.
// ATmega 8,16, 32 versions
// Adapter for Arduino UNO to measure capacity.
//---


A few pictures of prototype:


![C_meter.jpg](https://bitbucket.org/repo/j88j59/images/2366994987-C_meter.jpg)


First steps in phase tests

![phase_test.jpg](https://bitbucket.org/repo/j88j59/images/2960378938-phase_test.jpg)

example of measure

![diagrams.jpg](https://bitbucket.org/repo/j88j59/images/1592770757-diagrams.jpg)


PCB project from EasyEDA

https://easyeda.com/wegi1/C_meter_ATtiny2313-uLzx7tdaU


![maked_side_a.jpg](https://bitbucket.org/repo/j88j59/images/3441455783-maked_side_a.jpg)


![maked_side_B.png](https://bitbucket.org/repo/j88j59/images/2351813419-maked_side_B.png)